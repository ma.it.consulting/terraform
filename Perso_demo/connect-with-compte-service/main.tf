terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.46.0"
    }
  }
}

provider "google" {
    project = "moonlit-triumph-370607"
    region = "us-central1"
    zone = "us-central1-c"
    credentials = "keys.json"
}

resource google_storage_bucket "mohamed-demo-1780-neosoft" {
    name = "bucket-from-tf-using-mohamed-1780"
    location = "US"
    project = "moonlit-triumph-370607"         
}